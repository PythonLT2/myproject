import pytest
from django.test import Client


@pytest.mark.xfail()
def test_index(client):
    assert client.get('/').status_code == 200


@pytest.mark.xfail()
def test_user_registration(client: Client):
    """Task: #3"""
    assert client.get('/accounts/register').status_code == 200
    assert client.post('/accounts/register')
    # Send email
    assert client.get('/accounts/register/activate')
    # Some change
